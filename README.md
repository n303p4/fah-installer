# Do NOT run this on a system with a GUI, it will break your GUI

# fah-installer

Installs Folding@home on an Ubuntu Server machine with Nvidia GPUs, and sets
up fan and power limit control as well as basic config.

Also installs Photopsin, which is a stats panel for Folding@home.
