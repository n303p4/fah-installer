#!/bin/bash
set -Eeuxo pipefail

if [[ $(id -u) -ne 0 ]]; then
    echo "Please run this script as root or sudo."
    exit 1
fi
if pgrep Xorg || pgrep -i wayland || pgrep kwin || pgrep mutter; then
    echo "This script should NOT be run on a machine with a running desktop."
    echo "It will break your desktop and you will be sorry."
    exit 1
fi

echo "Do NOT run this script if you have a desktop environment."
echo "ONLY run this script on a headless server."
read -p "Press Enter to continue, or Ctrl+C to cancel."

echo "Setting up firewall ..."
ufw allow ssh
ufw allow http
ufw enable

echo "Installing Nvidia drivers ..."
apt-get -y install nvidia-headless-550 nvidia-utils-550 nvidia-opencl-dev python3-pynvml

echo "Setting up GPU power limit control ..."
cp set_gpu_power_limit /opt
cp set-gpu-power-limit.service /etc/systemd/system
systemctl enable set-gpu-power-limit

echo "Installing nvidia-settings and its dependencies (for fan speed control) ..."
apt install lxsession xinit xserver-xorg xserver-xorg-video-nvidia-550 nvidia-settings --no-install-recommends

echo "Setting up GPU fan speed control ..."
cp set-gpu-fan-speed /opt
cp set-gpu-fan-speed.service /etc/systemd/system
systemctl enable set-gpu-fan-speed

mkdir /etc/fahclient/
if [ -f config.xml ]; then
    echo "Copying config.xml ..."
    cp ./config.xml /etc/fahclient
else
    echo "Generating and copying config.xml ..."
    python3 generate_config.py
    cp ./config.xml /etc/fahclient
fi

systemctl disable lightdm

echo "Rebooting..."
reboot
