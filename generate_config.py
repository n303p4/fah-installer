"""Simple wizard to generate folding@home config"""

CONFIG_BOILERPLATE = """<config>
  <!-- Client Control -->
  <fold-anon v='true'/>

  <!-- HTTP Server -->
  <allow v='127.0.0.1'/>

  <!-- Slot Control -->
  <auto-conf v='false'/>
  <power v='full'/>

  <!-- User Information -->
  <passkey v='{0}'/>
  <team v='{1}'/>
  <user v='{2}'/>

  <!-- Web Server -->
  <web-allow v='127.0.0.1'/>

  <!-- Folding Slots -->
  <slot id='0' type='CPU'>
    {4}
    <client-type v='advanced'/>
    <max-packet-size v='big'/>
  </slot>{3}
</config>
"""
GPU_SLOT = """
  <slot id='{0}' type='GPU'>
    <client-type v='advanced'/>
    <max-packet-size v='big'/>
  </slot>"""
CPU_SLOT_PAUSED = "<paused v='true'/>"
CPU_SLOT_NOTPAUSED = "<cpus v='{0}'/>"

print("You can just hit Enter for any of the following if you don't want to fill them out.")
while True:
    user = input("User: ")
    if user and not user.replace("_", "a").isalnum():
        print("User must be alphanumeric (_ is also allowed)")
        continue
    team = input("Team: ")
    if team and not team.isdecimal():
        print("Team must be a positive integer.")
        continue
    passkey = input("Passkey: ")
    num_cpu_threads = input("Number of CPU threads (0 disables CPU folding): ")
    if num_cpu_threads and not num_cpu_threads.isdecimal():
        print("Number of CPU threads must be 0 or a positive integer.")
        continue
    num_cpu_threads = num_cpu_threads.lstrip("0")
    if not num_cpu_threads:
        cpu_slot = CPU_SLOT_PAUSED
    else:
        cpu_slot = CPU_SLOT_NOTPAUSED.format(num_cpu_threads)
    num_gpu_slots = input("Number of GPU slots (0 disables GPU folding): ")
    if num_gpu_slots and not num_gpu_slots.isdecimal():
        print("Number of GPU slots must be 0 or a positive integer.")
        continue
    num_gpu_slots = num_gpu_slots.lstrip("0")
    gpu_slots = ""
    if num_gpu_slots:
        num_gpu_slots = int(num_gpu_slots)
        for index in range(1, num_gpu_slots+1):
            gpu_slots += GPU_SLOT.format(index)
    confirm = input("Are you sure? [Y/n] ")
    if not confirm.lower() == "n":
        break
config = CONFIG_BOILERPLATE.format(passkey, team, user, gpu_slots, cpu_slot)
with open("config.xml", "w") as file_object:
    file_object.write(config)
