#!/bin/bash
set -Eeuxo pipefail

if [[ $(id -u) -ne 0 ]]; then
    echo "Please run this script as root or sudo."
    exit 1
fi

echo "Installing FAHClient ..."
wget https://download.foldingathome.org/releases/public/release/fahclient/debian-stable-64bit/v7.6/fahclient_7.6.21_amd64.deb -O fahclient.deb
dpkg -i fahclient.deb

echo "Installing photopsin ..."
git clone https://gitlab.com/n303p4/photopsin
cd photopsin
./install.sh
